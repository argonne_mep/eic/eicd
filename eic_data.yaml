---
options :
  # should getters / setters be prefixed with get / set?
  getSyntax: False
  # should POD members be exposed with getters/setters in classes that have them as members?
  exposePODMembers: True
  includeSubfolder: True

components :

  eic::VectorPxPyPzM:
    Members :
      - double px  
      - double py 
      - double pz 
      - double m

  eic::VectorXYZT:
    Members :
      - double x  
      - double y 
      - double z 
      - double t

  eic::VectorXYZ :
    Members :
      - double x  // x 
      - double y  // y
      - double z  // z

datatypes :



  eic::Particle :
    Description : "Basic Particle used for reconstructed and generated particles"
    Author : "W.Armstrong"
    Members :
      - eic::VectorPxPyPzM p       // Four momentum.
      - eic::VectorXYZT    v       // vertex.
      - long long          pid     // Particle type identification code
      - long long          status  // Status code

        #eic::SimCalorimeterHit:
        #  Description: "EIC simulated calorimeter hit"
        #  Author : "F.Gaede, B. Hegner"
        #  Members:
        #    - long long cellID0  // The detector specific (geometrical) cell id.
        #    - long long cellID1  // The second detector specific (geometrical) cell id.
        #    - float energy // The energy of the hit in [GeV].
        #    - float time   // The time of the hit in [ns].
        #    - std::array<float, 3> position // The position of the hit in world coordinates.


  eic::RawCalorimeterHit:
    Description: "Raw (digitized) calorimeter hit"
    Author : "W.Armstrong"
    Members:
      - long long cellID0   // The detector specific (geometrical) cell id.
      - long long cellID1   // The second detector specific (geometrical) cell id.
      - long long amplitude // The amplitude of the hit in ADC counts.
      - long long timeStamp // The time stamp for the hit.

  eic::CalorimeterHit:
    Description: "Calorimeter hit"
    Author : "W.Armstrong"
    Members:
      - long long cellID0  // The detector specific (geometrical) cell id.
      - long long cellID1  // The second detector specific (geometrical) cell id.
      - float     energy   // The energy of the hit in [GeV].
      - float     time     // The time of the hit in [ns].
      - eic::VectorXYZ position // The position of the hit in world coordinates.
      - int       type     // The type of the hit
    OneToOneRelations:
      - eic::RawCalorimeterHit rawHit // The RawCalorimeterHit

        #  eic::SimTrackerHit:
        #    Description: "EIC simulated tracker hit"
        #    Author : "F.Gaede, B. Hegner"
        #    Members:
        #      - long long cellID0  // The detector specific (geometrical) cell id.
        #      - long long cellID1  // The second detector specific (geometrical) cell id.
        #      - std::array<double, 3> position // The position of the hit in world coordinates.
        #      - float time   // The time of the hit in [ns].
        #      - float pathLength // path length
        #      - float EDep   // EDep
        #      - std::array<float, 3> _p // momentum
        #    OneToOneRelations:
        #      - eic::MCParticle particle // The MCParticle that caused the hit.

  eic::RawTrackerHit:
    Description : "Raw (digitized) tracker hit"
    Author : "W.Armstrong"
    Members:
      - long long cellID // The detector specific (geometrical) cell id.
      - int time    // tdc value.
      - int charge  // adc value

  eic::TrackerHit:
    Description : "Tracker hit (reconstructed from Raw)"
    Author : "W.Armstrong"
    Members :
      - long long cellID0 // The detector specific (geometrical) cell id.
      - long long cellID1 // The second detector specific (geometrical) cell id.
      - long long time    // The time of the hit.
      - float EDep        // EDep
      - float EDepError   // error on EDep
      - std::array<double, 3> position // position 
      - std::array<double, 3> covMatrix // covMatrix 

  eic::Cluster:
    Description: "EIC cluster"
    Author : "F.Gaede, B. Hegner"
    Members:
      - float energy // Energy of the cluster
      - std::array<float, 3> position // Position of the cluster.
      - std::array<float, 6> positionError // Covariance matrix of the position (6 Parameters)
      - float theta // Intrinsic direction of cluster at position - Theta.
      - float phi   // Intrinsic direction of cluster at position - Phi.
      - std::array<float, 3> directionError // Covariance matrix of the direction (3 Parameters)
        #- std::vector<float> shape  // Shape parameters
        #- std::vector<float> weight // weight of a particular cluster
        #- std::vector<float> subdetectorEnergies // A vector that holds the energy observed in a particular subdetector.
    OneToManyRelations:
      - eic::Cluster clusters // The clusters that have been combined to this cluster.
      - eic::CalorimeterHit hits // The hits that have been combined to this cluster.

  eic::MCParticle:
    Description: "EIC MC Particle"
    Author : "F.Gaede, B. Hegner"
    Members:
      - long long pdg // The PDG code of the particle.
      - long long genstatus   // The status for particles as defined by the generator.
      - std::array<double, 3> vertex // The production vertex of the particle in [mm].
      - float charge // The particle's charge.
      - float mass // The mass of the particle in [GeV]
      - float time // The creation time of the particle in [ns] wrt. the event, e.g. for preassigned decays or decays in flight from the simulator.
      - std::array<double, 3> endpoint // The endpoint of the particle in [mm]
      - bool endpointSet  // Whether the endpoint has been set
      - std::array<double, 3> momentum // The particle's 3-momentum at the production vertex in [GeV]
    OneToManyRelations:
      - eic::MCParticle parents // The parents of this particle.
      - eic::MCParticle daughters // The daughters this particle.
    ExtraCode :
      declaration: "
      double Px() const {return momentum().at(0);}\n
      double Py() const {return momentum().at(1);}\n
      double Pz() const {return momentum().at(2);}\n
      double Px2() const {return momentum().at(0)*momentum().at(0);}\n
      double Py2() const {return momentum().at(1)*momentum().at(1);}\n
      double Pz2() const {return momentum().at(2)*momentum().at(2);}\n
      //ROOT::Math::XYZTVector FourVector() const { return  ROOT::Math::XYZTVector(Px(),Py(),Pz(),std::sqrt(Px2()+Py2()+Pz2()+mass()*mass())); }\n
      //double theta() const { return  ROOT::Math::XYZVector(Px(),Py(),Pz()).Theta();}\n
      "

  eic::ReconstructedParticle:
    Description: "EIC Reconstructed Particle"
    Author : "F.Gaede, B. Hegner"
    Members:
      - int type     // Type of reconstructed particle.
      - double energy // Energy of the reconstructed particle.
      - std::array<double, 3> momentum // The reconstructed particle's 3-momentum
      - float charge // The particle's charge
      - double mass   // The mass of the particle in [GeV]
    OneToOneRelations:
      - eic::Vertex vertex // The start vertex associated to this particle.
    OneToManyRelations:
      - eic::Cluster clusters // The clusters combined to this particle.
      - eic::Track tracks // The tracks combined to this particle"
      - eic::ReconstructedParticle particles // The particles combined to this particle

  eic::Track:
    Description: "EIC reconstructed track"
    Author : "F.Gaede, B. Hegner"
    Members:
      - float chi2 // Chi2
      - int ndf    // Number of degrees of freedom of the track fit.
      - float dEdx // dEdx of the track.
      - float dEdxError // Error of dEdx.
      - float radiusOfInnermostHit // The radius of the innermost hit that has been used in the track fit.
      #- std::vector<int> subdetectorHitNumbers // The number of hits in particular subdetectors
    OneToManyRelations:
      - eic::Track tracks    // The tracks that have been combined to this track.
      - eic::TrackerHit hits // The hits that have been combined to this track.
      - eic::TrackState trackStates // Track states associated to this track.

  eic::TrackerData:
    Description : "EIC tracker data"
    Author : "F.Gaede, B. Hegner"
    Members:
      - long long cellID0 // The detector specific (geometrical) cell id.
      - long long cellID1 // The second detector specific (geometrical) cell id.
      - int time    // The time of the hit.
      - int charge  // adc value
        #- std::vector<float> charge // The corrected (calibrated) FADC spectrum.

  eic::TrackerPulse:
    Description : "EIC tracker pulse"
    Author : "F. Gaede, B. Hegner"
    Members:
      - long long cellID0  // The detector specific (geometrical) cell id.
      - long long cellID1  // The second detector specific (geometrical) cell id.
      - int quality  // ...
      - float time   // The time of the pulse.
      - float charge // The integrated charge of the pulse
        #      - std::vector<float> covMatrix    // ...
    OneToOneRelations:
      - eic::TrackerData corrData // ...

  eic::TrackerRawData:
    Description: "EIC tracker raw data"
    Author : "Whit Armstrong"
    Members:
      - long long cellID0  // The detector specific (geometrical) cell id.
      - long long cellID1  // The second detector specific (geometrical) cell id.
      - long long channelID  // channel  id.
      - int time     //  time measurement associated with the adc values.
      - int adc      // measured ADC values
        #      - std::vector<short> adc // measured ADC values.
  
  eic::TrackerData:
    Description: "EIC tracker data"
    Author : "F.Gaede, B. Hegner"
    Members:
      - long long cellID0  // The detector specific (geometrical) cell id.
      - long long cellID1  // The second detector specific (geometrical) cell id.
      - int time     //  time measurement associated with the adc values.
        #      - std::vector<short> charge // The actual FADC spectrum.

  # EIC TrackState
  eic::TrackState:
      Description: "EIC track state"
      Author : "F.Gaede, B. Hegner"
      Members:
        - int location // The location of the track state.
        - float d0     // Impact parameter of the track in (r-phi).
        - float phi    // Phi of the track at the reference point.
        - float omega  // Omega is the signed curvature of the track in [1/mm].
        - float z0     // Impact parameter of the track in (r-z).
        - float tanLambda // Lambda is the dip angle of the track in r-z at the reference point.
        - std::array<float, 3> referencePoint // Reference point of the track parameters
          #        - std::vector<float> covMatrix // Covariance matrix of the track parameters.

  eic::Vertex:
      Description: "EIC vertex"
      Author : "W.Armstrong"
      Members:
        - int primary       // Whether it is the primary vertex of the event
        - float chi2        // Chi squared of the vertex fit.
        - float probability // Probability of the vertex fit
        - eic::VectorXYZT position // postion and time of vertex.
          #        - std::vector<float> cov // <empty>
          #        - std::vector<float> par // <empty>
      OneToOneRelations:
        - eic::ReconstructedParticle particle // Reconstructed Particle associated to the Vertex.

